package main

import (
	"bufio"
	"encoding/base64"
	"fmt"
	"io"
	"log"
	"github.com/spacemonkeygo/openssl"
	"os"
	"strconv"
	"strings"
)

const (
	filenameRes = "results.txt"
)

/* structure to decode text */
type Crypter struct {
	key    []byte
	iv     []byte
	cipher *openssl.Cipher
}

/* function to handle error */
func handleError(err error) {

	/* handle error */
	if err != nil {
		log.Fatal(err)
	}

}

/* function to create new decoder */
func NewCrypter(key []byte, iv []byte, modDecr string) (*Crypter, error) {

	var (
		err    error
		cipher *openssl.Cipher
	)

	/* create new decoder */
	cipher, err = openssl.GetCipherByName(modDecr)
	if err != nil {
		return nil, err
	}

	/* return decoder */
	return &Crypter{key, iv, cipher}, nil

}

/* function to decode text */
func (c *Crypter) Decrypt(input []byte) ([]byte, error) {

	var (
		err           error
		ctx           openssl.DecryptionCipherCtx
		cipherbytes   []byte
		finalbytes    []byte
	)

	/* create context */
	ctx, err = openssl.NewDecryptionCipherCtx(c.cipher, nil, c.key, c.iv)
	if err != nil {
		return nil, err
	}

	/* do the first stage of decoding */
	cipherbytes, err = ctx.DecryptUpdate(input)
	if err != nil {
		return nil, err
	}

	/* do the second stage of decoding */
	finalbytes, err = ctx.DecryptFinal()
	if err != nil {
		return nil, err
	}

	/* combine intermediate results and send answer */
	cipherbytes = append(cipherbytes, finalbytes...)
	return cipherbytes, nil

}

/* function to decode text from base64 format */
func base64Decode(message []byte) ([]byte, error) {

	var (
		length int
		err    error
		answer []byte
	)

	/* decode base64 format */
	answer = make([]byte, base64.StdEncoding.DecodedLen(len(message)))
	length, err = base64.StdEncoding.Decode(answer, message)

	/* return answer */
	return answer[:length], err

}

/* function to write message and read row */
func readRow(mes string, row *string) {

	var (
		err error
	)

	/* function to read row and write message */
	fmt.Println(mes)
	_, err = fmt.Scanln(row)
	handleError(err)

}

func handleKeys(key string, modDecr string) []byte {

	var (
		answer []byte
		i      int
		length int
	)

	/* set length */
	if modDecr == "aes-256-cfb" {
		length = 32
	} else if modDecr == "aes-128-ctr" {
		length = 16
	}

	/* create answer byte array */
	answer = make([]byte, length)

	/* fill byte array using "key" string or "zero" symbol */
	i = 0
	for i < length {
		if i < len(key) {
			answer[i] = key[i]
		} else {
			answer[i] = 0
		}
		i++ 
	}

	/* return answer */
	return answer

}

/* function to write message and read bytes */
func readBytes(mes string, bytes *[]byte) {

	var (
		num   int
		err   error
		row   string
		words []string
		i     int
		scan  *bufio.Scanner
	)

	/* write message */
	fmt.Println(mes)

	/* read row and handle error */
	scan = bufio.NewScanner(os.Stdin)
	scan.Scan()
	row = scan.Text()
	handleError(err)

	/* split row */
	words = strings.Split(row," ")
	/* create array of bytes */
	*(bytes) = make([]byte, len(words))

	/* handler all words */
	i = 0
	for i < len(words) {
		/* convert word into numbers */
		num, err = strconv.Atoi(words[i])
		handleError(err)
		/* save results */
		(*bytes)[i] = byte(num)
		i++
	}

}

/* function to read file and return full row */
func readFile(filename string) string {

	var (
		file   *os.File
		err    error
		reader *bufio.Reader
		row    string
		line   []byte
	)

	/* open file */
	file, err = os.Open(filename)
	handleError(err)

	/* create reader */
	reader = bufio.NewReader(file)
	row = ""
	for {

		/* read one line */
		line, _, err = reader.ReadLine()
		if err == io.EOF {
			break
		} else if err != nil {
			log.Fatal(err)
		}

		/* convert to string */
		row += string(line)

	}

	/* return row */
	return row

}

/* function to write results */
func writeResults(res []byte) {

	var (
		file *os.File
		err  error
	)

	/* create file */
	file, err = os.Create(filenameRes)
	handleError(err)

	/* write file */
	_, err = file.Write(res)
	handleError(err)

}

/* entry point */
func main() {

	var (
		key, iv   string
		ivBytes   []byte
		keyBytes  []byte
		crypter   *Crypter
		filename  string
		err       error
		row       string
		modDecr   string
		modVect   string
		encArray  []byte
		decArray  []byte
	)

	/* read parameters */
	readRow("Key for decryption:", &key)
	readRow("Mode of initial vector:", &modVect)
	if modVect == "text" {
		readRow("Initial vector (text) for decryption:", &iv)
	} else if modVect == "byte" {
		readBytes("Initial vector (bytes) for decryption:", &ivBytes)
	}
	readRow("Filename for decryption:", &filename)
	readRow("Mode of decryption:", &modDecr)
	/* read row from file */
	row = readFile(filename)

	/* decode text from base64 format */
	encArray, err = base64Decode([]byte(row))
	handleError(err)

	/* handle keys */
	keyBytes = handleKeys(key, modDecr)

	/* create decoder */
	if modVect == "text" {
		crypter, err = NewCrypter(keyBytes, []byte(iv), modDecr)
	} else if modVect == "byte" {
		crypter, err = NewCrypter(keyBytes, ivBytes, modDecr)
	}
	handleError(err)

	/* decode text and write result */
	decArray, err = crypter.Decrypt(encArray)
	handleError(err)
	writeResults(decArray)

}